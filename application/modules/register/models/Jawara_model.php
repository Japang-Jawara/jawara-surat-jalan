<?php
class Jawara_model extends CI_Model{

    public function getConfig(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SSJ')->get()->result();
    }

    public function getSku(){
        return $this->db->select('*')->from('jawara_sku')->order_by('sku', 'ASC')->get()->result();
    }

    public function cekDeliverykode()
    {
        $query = $this->db->query("SELECT MAX(delivery_kode) as delivery_kode from delivery WHERE delivery_kode LIKE '%SRJ%'");
        $hasil = $query->row();
        return $hasil->delivery_kode;
    }

    public function cekDelivery($deliveryKode){
        return $this->db->select('*')->from('delivery')->where('delivery_kode', $deliveryKode)->get()->result();
    }

    public function cekDeliveryDetail($deliveryKode){
        return $this->db->select('*')->from('delivery_detail')->where('delivery_kode', $deliveryKode)->get()->result();
    }

    public function leds($data){
        $result = $this->db->insert('delivery', $data);

        if($result){
            $this->session->set_flashdata('success','SURAT JALAN BERHASIL DISIMPAN');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','SURAT JALAN GAGAL DISIMPAN');
            return FALSE;
        }
    }

    public function reveral($data){
        $result = $this->db->insert('delivery', $data);

        if($result){
            $this->session->set_flashdata('success','SURAT JALAN BERHASIL DIREVERSAL');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','SURAT JALAN GAGAL DIREVERSAL');
            return FALSE;
        }
    }

    public function setUserName($userName) 
	{
        return $this->_userName = $userName;
	}

	public function getAllUsers()
	{
		$this->db->select(array('c.id as jawara_id', 'c.name as jawara_name', 'c.alamat_usaha as jawara_alamat', 'c.phone as jawara_phone', 'c.shop_name_dis as jawara_toko'));
		$this->db->from('jawara_leads as c');
        $this->db->like('UPPER (c.name)', $this->_userName, 'both');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllUsersBtob()
	{
		$this->db->select(array('c.id as btob_id', 'c.name as btob_name', 'c.address as btob_alamat'));
		$this->db->from('tbl_btob as c');
        $this->db->like('UPPER (c.name)', $this->_userName, 'both');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllUsersReversal()
	{
        $this->db->select(array('*'));
		$this->db->from('delivery as c');
        $this->db->like('UPPER (c.delivery_kode)', $this->_userName, 'both');
        $this->db->where('c.status_reversal IS NULL');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPrice($sku, $tanggal){
        $a = $this->db->query("SELECT * FROM tbl_price WHERE sku = '$sku' AND date <= '$tanggal' ORDER BY id DESC LIMIT 1")->result_array();

        if($a){
            return $a;
        }else{
            return $this->db->query("SELECT * FROM tbl_price WHERE sku = '$sku' AND date >= '$tanggal' ORDER BY id ASC LIMIT 1")->result_array();
        }
    }

}
?>