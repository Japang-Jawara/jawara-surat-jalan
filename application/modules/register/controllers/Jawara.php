<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawara extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Server_model', 'modelCommerce');
          $this->load->model('Jawara_model', 'modelJawara');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
     }

     function index(){
          if($this->session->userdata('is_deploy') != NULL) {
                
               $x['sku'] = $this->modelJawara->getSku();
               $x['ssj'] = $this->modelJawara->getConfig();

               $this->load->view("include/frontend/head");
               $this->load->view('index', $x);
               $this->load->view("include/frontend/footer");
               $this->load->view("include/alert");
          }else{
               redirect();
          }
     }

     function btob(){
          if($this->session->userdata('is_deploy') != NULL) {
               $x['sku'] = $this->modelJawara->getSku();
               $x['ssj'] = $this->modelJawara->getConfig();

               $this->load->view("include/frontend/head");
               $this->load->view('btob', $x);
               $this->load->view("include/frontend/footer");
               $this->load->view("include/alert");
          }else{
               redirect();
          }
     }

     function reversal(){
          if($this->session->userdata('is_deploy') != NULL) {
               $this->load->view("include/frontend/head");
               $this->load->view('reversal');
               $this->load->view("include/frontend/footer");
               $this->load->view("include/alert");
          }else{
               redirect();
          }
     }

     //COMMERCE
     public function getInvoiceAutocomplete() 
	{
          $json = array();
          $nomorInvoice =  $this->input->post('query');
          $this->modelCommerce->setNomorInvoice($nomorInvoice);
          $getInvoice = $this->modelCommerce->getAllInvoice();
          foreach ($getInvoice as $element) {
               $json[] = array(
                    'invoice_no' => $element['invoice_no'],
                    'name' => $element['name'],
               );
          }
		echo json_encode($json);
	}
     //END COMMERCE

     public function delivery(){

          $this->form_validation->set_rules('delivery_customer', 'Nama Jawara', 'required');

          if ($this->form_validation->run() == FALSE)
          {
               $this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
               redirect();
          }
          else
          {  
               $email = $this->input->post('delivery_email');
               $cekAnggota = explode('@', $email);
              
               if($cekAnggota[1] == 'jaringpangan.com'){

                    $dariDB = $this->modelJawara->cekDeliverykode();

                    $urutan = 0;
                    
                    if(substr($dariDB, 3, 5) < 01000){
                         $urutan = (int) substr($dariDB, 5, 5);
                    }else{
                         $urutan = substr($dariDB, 3, 5);
                    }

                    // contoh SRJ0001, angka 3 adalah awal pengambilan angka, dan 4 jumlah angka yang diambil
                    $kodeDeliverySekarang = $urutan + 1;
                    $kodedel = 'SRJ'.sprintf("%05s", $kodeDeliverySekarang);

                    $jumlah = count($this->input->post('product'));
                    
                    $sum = 0;

                    for($i=0;$i<$jumlah;$i++){
                         $data = array(
                              'delivery_kode'=> $kodedel,
                              'sku'=> $this->input->post('product')[$i],
                              'jumlah'=> $this->input->post('jumlah')[$i],
                              'price'=> $this->input->post('price')[$i],
                              'total'=> $this->input->post('total')[$i]
                         );
                         $this->db->insert("delivery_detail",$data);
                         $sum += $this->input->post('total')[$i];
                    }

                    $x['jawara_id'] = $this->input->post('autocomplete');
                    $x['delivery_kode'] = $kodedel;
                    $x['delivery_email'] = $this->input->post('delivery_email');
                    $x['delivery_tanggal'] = $this->input->post('delivery_tanggal');
                    $x['delivery_customer'] = $this->input->post('delivery_customer');
                    $x['delivery_address'] = $this->input->post('delivery_address');
                    $x['delivery_toko'] = $this->input->post('delivery_toko');
                    $x['delivery_phone'] = $this->input->post('delivery_phone');
                    $x['delivery_no'] = $this->input->post('delivery_no');
                    $x['delivery_mobil'] = $this->input->post('delivery_mobil');
                    $x['delivery_ket'] = $this->input->post('delivery_ket');
                    $x['delivery_status'] = $this->input->post('delivery_status');
                    $x['delivery_progress'] = 'ON PROGRESS';
                    $x['delivery_progress_at'] = date('Y-m-d H:i:s');
                    $x['invoice_no'] = $this->input->post('invoice_no');
                    $x['total_price'] = $sum;
                    $x['status'] = 'JWR';
                    $x['created_at'] = date('Y-m-d H:i:s');

                    $this->modelJawara->leds($x);

                    redirect('register/jawara#daftar');

               }else{
                    $this->session->set_flashdata('success','Tanyakan Ke TIM Tech Kenapa Email nya Tidak Bisa Digunakan');
                    redirect('register/jawara#daftar');
               }
          }        

     }

     public function getUserAutocomplete() 
	{
          $json = array();
          $userName =  $this->input->post('query');
          $this->modelJawara->setUserName($userName);
          $geUsers = $this->modelJawara->getAllUsers();
          foreach ($geUsers as $element) {
               $json[] = array(
                    'jawara_id' => $element['jawara_id'],
                    'jawara_name' => $element['jawara_name'],
                    'jawara_alamat' => $element['jawara_alamat'],
                    'jawara_phone' => $element['jawara_phone'],
                    'jawara_toko' => $element['jawara_toko'],
               );
          }
		echo json_encode($json);
	}

     //BTOB
     public function getUserAutocompleteBtob() 
	{
          $json = array();
          $userName =  $this->input->post('query');
          $this->modelJawara->setUserName($userName);
          $geUsers = $this->modelJawara->getAllUsersBtob();
          foreach ($geUsers as $element) {
               $json[] = array(
                    'btob_id' => $element['btob_id'],
                    'btob_name' => $element['btob_name'],
                    'btob_alamat' => $element['btob_alamat'],
               );
          }
		echo json_encode($json);
	}

     public function deliveryBtob(){

          $this->form_validation->set_rules('delivery_customer', 'Nama Jawara', 'required');

          if ($this->form_validation->run() == FALSE)
          {
               $this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
               redirect();
          }
          else
          {  
               $email = $this->input->post('delivery_email');
               $cekAnggota = explode('@', $email);
              
               if($cekAnggota[1] == 'jaringpangan.com'){

                    $dariDB = $this->modelJawara->cekDeliverykode();

                    $urutan = 0;
                    
                    if(substr($dariDB, 3, 5) < 01000){
                         $urutan = (int) substr($dariDB, 5, 5);
                    }else{
                         $urutan = substr($dariDB, 3, 5);
                    }

                    // contoh SRJ0001, angka 3 adalah awal pengambilan angka, dan 4 jumlah angka yang diambil
                    $kodeDeliverySekarang = $urutan + 1;
                    $kodedel = 'SRJ'.sprintf("%05s", $kodeDeliverySekarang);
                    
                    $jumlah = count($this->input->post('product'));

                    for($i=0;$i<$jumlah;$i++){
                         $data = array(
                              'delivery_kode'=> $kodedel,
                              'sku'=> $this->input->post('product')[$i],
                              'jumlah'=> $this->input->post('jumlah')[$i]
                         );
                         $this->db->insert("delivery_detail",$data);
                    }

                    $x['delivery_kode'] = $kodedel;
                    $x['delivery_email'] = $this->input->post('delivery_email');
                    $x['delivery_tanggal'] = $this->input->post('delivery_tanggal');
                    $x['delivery_customer'] = $this->input->post('delivery_customer');
                    $x['delivery_address'] = $this->input->post('delivery_address');
                    $x['delivery_no'] = $this->input->post('delivery_no');
                    $x['delivery_mobil'] = $this->input->post('delivery_mobil');
                    $x['delivery_ket'] = $this->input->post('delivery_ket');
                    $x['delivery_status'] = $this->input->post('delivery_status');
                    $x['delivery_progress'] = 'ON PROGRESS';
                    $x['delivery_progress_at'] = date('Y-m-d H:i:s');
                    $x['invoice_no'] = $this->input->post('invoice_no');
                    $x['status'] = 'B2B';
                    $x['created_at'] = date('Y-m-d H:i:s');

                    $this->modelJawara->leds($x);

                    redirect('btob#daftar');

               }else{
                    $this->session->set_flashdata('success','Tanyakan Ke TIM Tech Kenapa Email nya Tidak Bisa Digunakan');
                    redirect('btob#daftar');
               }
          }        

     }

     public function getPrice(){

          $product = $this->input->post('product');
          $tanggal = $this->input->post('tanggal');

          $geUsers = $this->modelJawara->getPrice($product, $tanggal);

          foreach ($geUsers as $element) {
               $json = array(
                    'price' => $element['price'],
               );
          }
		echo json_encode($json);
     }

     public function getUserAutocompleteReversal() 
	{
          $json = array();
          $userName =  $this->input->post('query');
          $this->modelJawara->setUserName($userName);
          $geUsers = $this->modelJawara->getAllUsersReversal();
          foreach ($geUsers as $element) {
               $json[] = array(
                    'delivery_kode' => $element['delivery_kode'],
                    'delivery_customer' => $element['delivery_customer'],
                    'delivery_no' => $element['delivery_no'],
               );
          }
		echo json_encode($json);
	}

     public function reveral(){

          $this->form_validation->set_rules('delivery_kode', 'Delivery Kode', 'required');

          if ($this->form_validation->run() == FALSE)
          {
               $this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
               redirect();
          }
          else
          {  
               $deliveryKode = $this->input->post('delivery_kode');

               $delivery = $this->modelJawara->cekDelivery($deliveryKode);
               $deliveryDetail = $this->modelJawara->cekDeliveryDetail($deliveryKode);

               foreach($deliveryDetail as $r){
                    $data = array(
                         'delivery_kode'=> $r->delivery_kode.'-R',
                         'sku'=> $r->sku,
                         'jumlah'=> '-'.$r->jumlah,
                         'price'=> '-'.$r->price,
                         'total'=> $r->total
                    );
                    $this->db->insert("delivery_detail",$data);
               }

               $x['jawara_id'] = $delivery[0]->jawara_id;
               $x['delivery_kode'] = $deliveryKode.'-R';
               $x['delivery_email'] = $this->input->post('delivery_email');
               $x['delivery_tanggal'] = $delivery[0]->delivery_tanggal;
               $x['delivery_customer'] = $delivery[0]->delivery_customer;
               $x['delivery_phone'] = $delivery[0]->delivery_phone;
               $x['delivery_address'] = $delivery[0]->delivery_address;
               $x['delivery_toko'] = $delivery[0]->delivery_toko;
               $x['delivery_no'] = $delivery[0]->delivery_no;
               $x['delivery_mobil'] = $delivery[0]->delivery_mobil;
               $x['delivery_ket'] = $delivery[0]->delivery_ket;
               $x['delivery_ket_reversal'] = $this->input->post('delivery_ket_reversal');
               $x['total_price'] = '-'.$delivery[0]->total_price;
               $x['status'] = $delivery[0]->status;
               $x['delivery_status'] = $delivery[0]->delivery_status;
               $x['delivery_progress'] = $delivery[0]->delivery_progress;
               $x['delivery_progress_at'] = $delivery[0]->delivery_progress_at;
               $x['delivery_progress_up'] = $delivery[0]->delivery_progress_up;
               $x['invoice_no'] = $delivery[0]->invoice_no;
               $x['status_reversal'] = '1';
               $x['created_at'] = date('Y-m-d H:i:s');

               $this->modelJawara->reveral($x);
               
               $u['status_reversal'] = '1';
               $this->db->where('delivery_kode', $deliveryKode)->update('delivery', $u);

               redirect('reversal#daftar');

          }        

     }

}