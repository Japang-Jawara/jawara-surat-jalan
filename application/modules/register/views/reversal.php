
	<section class="page-section m-5" id="daftar">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">JApang WArung RAkyat</h4>
				<p class="section-subheading text-muted">Yuk Buat Surat Jalan.</p>
            </div>
			<div class="row text-center">
				<form action="register/jawara/reveral" method="POST" name="form-wizard" class="form-control-with-bg" enctype="multipart/form-data" onsubmit="return validateForm()">
					<input type="hidden" name="autocomplete" id="field-autocomplete">
					<!-- begin wizard -->
					<div class="row">
						<!-- begin col-8 -->
						<div class="col-xl-8 offset-xl-2">
							<br>

							<h5 style="text-align: left">DATA</h5>
							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Email Verifikator<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="email"  name="delivery_email" id="delivery_email" value="<?= $this->session->userdata('email_address') ?>" placeholder="Email Verifikator" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" readonly required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Kode Delivery <span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="text" name="delivery_kode" id="delivery_kode" placeholder="Delivery Kode" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Nama Klien <span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="text" name="delivery_customer" id="delivery_customer" placeholder="Nama Klien" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" readonly/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Nomor Surat <span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="text" name="delivery_no" id="delivery_no" placeholder="Nomor Surat" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" readonly/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Keterangan<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<textarea name="delivery_ket_reversal" id="delivery_ket_reversal" placeholder="Keterangan" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required></textarea>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="submit" class="btn btn-success">Reversal</button>
								</div>
							</div>
							
							<br>
							<!-- <center><button type="submit" class="btn btn-primary btn-sm center-block mb-3">SIMPAN</button></center> -->
						</div>
						<!-- end col-8 -->
					</div>
					<!-- end wizard -->
				</form>	
			</div>
		</div>
	</section>

	

    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
	
	<script>
	function validateForm() {
		let x = document.forms["form-wizard"]["autocomplete"].value;
		if (x == "") {
			alert("Kode Delivery Tidak Ditemukan");
			return false;
		}
	}
	</script>

	<script>
	// autocomplete functionality
	if (jQuery('input#delivery_kode').length > 0) {
		jQuery('input#delivery_kode').typeahead({
		displayText: function(item) {
			return item.delivery_kode
		},
		afterSelect: function(item) {
			this.$element[0].value = item.delivery_kode
			
			jQuery("input#field-autocomplete").val(item.delivery_kode);
			jQuery("input#delivery_customer").val(item.delivery_customer);
			jQuery("input#delivery_no").val(item.delivery_no);
		},
		source: function (query, process) {
			jQuery.ajax({
					url: '<?= base_url(); ?>register/jawara/getUserAutocompleteReversal',
					data: {query:query.toUpperCase()},
					dataType: "json",
					type: "POST",
					success: function (data) {
						process(data)
					}
				})
		}   
		});
	}

	</script>
