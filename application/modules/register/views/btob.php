
	<section class="page-section m-5" id="daftar">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">JApang WArung RAkyat</h4>
				<p class="section-subheading text-muted">Yuk Buat Surat Jalan.</p>
            </div>
			<div class="row text-center">
				<form action="register/jawara/deliveryBtob" method="POST" name="form-wizard" class="form-control-with-bg" enctype="multipart/form-data">
					<input type="hidden" name="autocomplete" id="field-autocomplete">
					<input type="hidden" name="autocompletecommerce" id="field-autocompletecommerce">
					<!-- begin wizard -->
					<div class="row">
						<!-- begin col-8 -->
						<div class="col-xl-8 offset-xl-2">
							<br>

							<h5 style="text-align: left">DATA</h5>
							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Email Verifikator<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="email"  name="delivery_email" id="delivery_email" value="<?= $this->session->userdata('email_address') ?>" placeholder="Email Verifikator" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" readonly required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Status Delivery<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<select class="form-control" name="delivery_status" id="delivery_status" data-parsley-group="step-1" data-parsley-required="true" required>
										<option value="">-- Pilih Status --</option>
										<?php foreach($ssj as $r) { ?>
											<option value="<?= $r->name ?>"><?= $r->name ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">No Invoice<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="text" name="invoice_no" id="invoice_no" placeholder="Isikan Nomor Invoice" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Tanggal<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="date"  name="delivery_tanggal" id="delivery_tanggal" value="<?= date('Y-m-d') ?>" placeholder="Tanggal Delivery" data-parsley-group="step-1" data-parsley-required="true" class="form-control" required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Nama B2B<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="text" name="delivery_customer" id="delivery_customer" placeholder="Isikan Nama B2B" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Alamat B2B<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<textarea name="delivery_address" id="delivery_address" placeholder="Alamat B2B" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required></textarea>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Nomor Surat Jalan<span class="text-danger"> *</span></label>
								<div class="col-lg-5 col-xl-6">
									<input type="text" name="delivery_no" id="delivery_no" placeholder="Nomor Surat Jalan" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Nomor Mobil<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<input type="text" name="delivery_mobil" id="delivery_mobil" placeholder="Nomor Mobil" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
								</div>
							</div>

							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">Keterangan<span class="text-danger"> *</span></label>
								<div class="col-lg-6 col-xl-6">
									<textarea name="delivery_ket" id="delivery_ket" placeholder="Keterangan" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required></textarea>
								</div>
							</div>

							<h5 style="text-align: left">SKU</h5>
							<div class="form-group row m-b-10">
								<label class="col-lg-5 text-lg-right col-form-label">SKU 1<span class="text-danger"> *</span></label>
								<div class="col-lg-3 col-xl-3">
									<select class="form-select" name="product[]" id="product1" aria-label="Default select example" required>
										<option value="" selected>PILIH SKU</option>
										<?php foreach ($sku as $r): $val = $r->sku; $apa = $r->sku.' ('.$r->product.' '.$r->type.')';?>

										<option value="<?php echo $val ?>"><?php echo $apa ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-lg-3 col-xl-3">
									<input type="text" pattern="[0-9]{1,50}" name="jumlah[]" id="jumlah1" placeholder="JUMLAH" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
								</div>
							</div>
							
							<div class="ln_solid"></div>
							<div id="nextkolom" name="nextkolom"></div>
							<button type="button" id="jumlahkolom" value="1" style="display:none"></button>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="button" class="btn btn-info tambah-form">Tambah Form</button>
								<button type="submit" class="btn btn-success">Simpan</button>
								</div>
							</div>
							
						</div>
						<!-- end col-8 -->
					</div>
					<!-- end wizard -->
				</form>	
			</div>
		</div>
	</section>

    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

	<script>

	$("#delivery_status").on("change paste keyup", function() {
		if($(this).val() == '1ST DEPLOY'){
			jQuery("input#invoice_no").val(<?php echo time() ?>);
		}else if($(this).val() == 'RETURN'){
			jQuery("input#invoice_no").val(<?php echo time() ?>);
		}else{
			jQuery("input#invoice_no").val('');
		}
	});

	// autocomplete functionality
	if (jQuery('input#delivery_customer').length > 0) {
		jQuery('input#delivery_customer').typeahead({
		items : 1000,
		displayText: function(item) {
			return item.btob_name
		},
		afterSelect: function(item) {
			this.$element[0].value = item.btob_name
			
			jQuery("input#field-autocomplete").val(item.btob_id);
			jQuery("textarea#delivery_address").val(item.btob_alamat);
		},
		source: function (query, process) {
			jQuery.ajax({
					url: '<?= base_url(); ?>register/jawara/getUserAutocompleteBtob',
					data: {query:query.toUpperCase()},
					dataType: "json",
					type: "POST",
					success: function (data) {
						process(data)
					}
				})
		}   
		});
	}

	$(document).ready(function() {
		var i=2;
		$(".tambah-form").on('click', function(){        
			row =
				'<div class="rec-element">'+
					'<div class="form-group row m-b-10">'+
						'<label class="col-lg-5 text-lg-right col-form-label">SKU '+i+'<span class="text-danger"> *</span></label>'+
						'<div class="col-lg-3 col-xl-3">'+
							'<select class="form-select" name="product[]" id="product'+i+'" aria-label="Default select example" required>'+
								'<option value="" selected>PILIH SKU</option>'+
								<?php foreach ($sku as $r): $val = $r->sku; $apa = $r->sku.' ('.$r->product.' '.$r->type.')';?>

								'<option value="<?php echo $val ?>"><?php echo $apa ?></option>'+
								<?php endforeach; ?>
							'</select>'+					
						'</div>'+
						'<div class="col-lg-3 col-xl-3">'+
							'<span>'+
								'<input type="text" pattern="[0-9]{1,50}" name="jumlah[]" id="jumlah'+i+'" placeholder="JUMLAH" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>'+
							'</span>'+
							'<span class="input-group-btn">'+
								'<button type="button" class="btn btn-warning del-element"><i class="fa fa-minus-square"></i> Hapus</button>'+
							'</span>'+
						'</div>'+
					'</div>'+
					'<div class="ln_solid"></div>'+
                
                '</div>'
				;
			$(row).insertBefore("#nextkolom");

			$('#jumlahkolom').val(i+1);

			
			i++;        
		});

		$(document).on('click','.del-element',function (e) {        
			e.preventDefault()
			i--;
			//$(this).parents('.rec-element').fadeOut(400);
			$(this).parents('.rec-element').remove();
			$('#jumlahkolom').val(i-1);
		});        
	});

	// Commerce
	// autocomplete functionality
	if (jQuery('input#invoice_no').length > 0) {
		jQuery('input#invoice_no').typeahead({
		
		// Limit Show Name
		items : 1000,
		// Limit End Show Name

		displayText: function(item) {
			return item.invoice_no + ' | ' + item.name
		},
		afterSelect: function(item) {
			this.$element[0].value = item.invoice_no
			
			jQuery("input#field-autocompletecommerce").val(item.invoice_no);
			jQuery("input#invoice_name").val(item.name);
		},
		source: function (query, process) {
			jQuery.ajax({
					url: '<?= base_url(); ?>register/jawara/getInvoiceAutocomplete',
					data: {query:query.toUpperCase()},
					dataType: "json",
					type: "POST",
					success: function (data) {
						process(data)
					}
				})
		}   
		});
	}

	</script>
