<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Surat Jalan | Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- Favicon  -->
    <link rel="icon" href="<?php echo base_url('assets/img/logo/japang.png'); ?>">
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/default/app.min.css');?>" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-with-news-feed">
			<!-- begin news-feed -->
			<div class="news-feed">
				<div class="news-image" style="background-image: url(https://jaringpangan.com/wp-content/uploads/2021/11/Shutterstock_586540817-web-2-scaled-1.jpg)"></div>
				<div class="news-caption">
					<h4 class="caption-title"><b>Jaring</b> Pangan Indonesia</h4>
					<p>
						Aplikasi Surat Jalan.
					</p>
				</div>
			</div>
			<!-- end news-feed -->
			<!-- begin right-content -->
			<div class="right-content">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
						<span class="logo"></span> <b>Jaring</b> Pangan
						<small>Aplikasi Surat Jalan</small>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in-alt"></i>
					</div>
				</div>
				<!-- end login-header -->
                
				<!-- begin login-content -->
				<div class="login-content">
					<form action="login/proses" method="POST" class="margin-bottom-0">
						<div class="form-group m-b-15">
							<input type="text" name="email" class="form-control form-control-lg" placeholder="Email Address" required />
						</div>
						<div class="form-group m-b-15">
							<input type="password" name="password" class="form-control form-control-lg" placeholder="Password" data-toggle="password" required />
						</div>
						<div class="login-buttons">
							<button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
						</div>
						<hr />
						<p class="text-center text-grey-darker mb-0">
							&copy; JaPang Tech <?=date('Y')?>
						</p>
					</form>
				</div>
				<!-- end login-content -->
			</div>
			<!-- end right-container -->
		</div>
		<!-- end login -->
    </div>
    <!-- begin theme-panel -->
		
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('assets/js/app.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/theme/default.min.js');?>"></script>
	<!-- ================== END BASE JS ================== -->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>

	<script type="text/javascript">
		$("#password").password('toggle');
	</script>

</body>
</html>
