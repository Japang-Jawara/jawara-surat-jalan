<?php
class Glogin_model extends CI_Model {

    public function proses($email, $password){
        $this->db->select('*');
        $this->db->from('jawara_users');
        $this->db->where('email_address', $email);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data_user = $query->row();

            $this->db->where('email_address', $email);
            $this->db->where("password='$password'");

            $result = $this->db->get('jawara_users')->result();

            if (!empty($result)) {
                $this->session->set_userdata('first_name', $data_user->first_name);
                $this->session->set_userdata('last_name', $data_user->last_name);
                $this->session->set_userdata('email_address', $data_user->email_address);
                $this->session->set_userdata('profile_picture', $data_user->profile_picture);
                $this->session->set_userdata('role', $data_user->role);
                
                if($data_user->role == '997'){
                    $this->session->set_userdata('is_deploy', TRUE);
                    $this->session->set_flashdata('success', 'Welcome to Aplikasi Surat Jalan');
                    return TRUE;
                }else{
                    $this->session->set_flashdata('success', 'Anda Tidak Mempunyai Akses Ke Aplikasi Surat Jalan');
                    return FALSE;
                }
                
                
            } else {
                $this->session->set_flashdata('success', 'Password Anda Salah!!!');
                return FALSE;
            }
        } else {
            $this->session->set_flashdata('success', 'Email Anda Salah!!!');
            return FALSE;
        }

    }
}
?>
