<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

     function __construct(){
          parent::__construct();
		$this->load->model('glogin_model');
     }

     function index(){
          // $this->load->view('index');
          // $this->load->view("include/alert");
          redirect();
     }

     public function proses(){
		$email = $this->input->post('email', TRUE);
		$password = MD5($this->input->post('password', TRUE));

          if ($this->glogin_model->proses($email, $password)) {

               if($this->session->userdata('is_deploy') == TRUE){
                    redirect('register/jawara');
               }else{
                    redirect('login');
               }
          } else {
               redirect('login');
          }		
	}

	function logout()
	{
          $this->session->sess_destroy();
		$this->session->set_flashdata('success', 'Anda Berhasil Logout!');
		redirect();
	}

}