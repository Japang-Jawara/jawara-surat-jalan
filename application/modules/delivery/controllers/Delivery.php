<?php
defined('BASEPATH') or exit('No direct script access allowed ');

require('./application/libraries/pdf/TCPDF.php');

class DeliveryPDF extends TCPDF
{

	public function Header()
	{
		$image_file = "<img src=\"assets/img/logo/logojapang.png\" width=\"60\" height=\"15\"/>";
		$this->SetY(10);
		$isi_header = "
		<table align=\"right\">
			<tr>
				<td>" . $image_file . "</td>
			</tr>
		</table>
		<table align=\"right\">
			<tr>
				<td align=\"center\"><h4>SURAT JALAN</h4></td>
			</tr>
			<tr>
				<td align=\"center\"><p style=\"font-size:10px\">PT. Jaring Pangan Indonesia, Sovereign Plaza 12th Floor, Unit E</p></td>
			</tr>
			<tr>
				<td align=\"center\"><p style=\"font-size:10px\">Jakarta Selatan - 12430</p></td>
			</tr>
		</table>";
		$this->writeHTML($isi_header, true, false, false, false, '');
	}

	public function Footer()
	{
		// $image_file = "<img src=\"assets/pdf/laporanpengaduan.png\" width=\"180\" height=\"70\"/>";
		// $this->SetY(-40);
		// $this->writeHTML($image_file, true, false, false, false, '');
		// $this->SetY(-15);
		// $this->writeHTML("<hr>", true, false, false, false, '');
		// $this->SetFont('helvetica', '', 12);
		// $this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

class Delivery extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//LOAD MODELS
        $this->load->model('Delivery_model', 'modelDelivery');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

    public function index(){

        if($this->session->userdata('is_deploy') == TRUE){
			$x['ssjp'] = $this->modelDelivery->getConfigProgress();

			$this->load->view("include/frontend/head");
			$this->load->view('delivery', $x);
			$this->load->view("include/frontend/footer");
			$this->load->view("include/alert");
	   }else{
			$this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
			redirect('index.php/login');
	   }

    }

	public function detail(){

        if($this->session->userdata('is_deploy') == TRUE){

			$x['date'] = $this->input->post('delivery_tanggall');
			$x['data'] = $this->modelDelivery->getDelivery($x['date']);
			$x['ssjp'] = $this->modelDelivery->getConfigProgress();

			$this->load->view("include/frontend/head");
			$this->load->view('detail', $x);
			$this->load->view("include/frontend/footer");
			$this->load->view("include/alert");
	   }else{
			$this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
			redirect('index.php/login');
	   }

    }

	public function view(){

        if($this->session->userdata('is_deploy') == TRUE){

			$x['nomor'] = $this->input->get('nomor');
			$x['data'] = $this->modelDelivery->getDeliveryDetail($x['nomor']);

			$this->load->view("include/frontend/head");
			$this->load->view('view', $x);
			$this->load->view("include/frontend/footer");
			$this->load->view("include/alert");
	   }else{
			$this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
			redirect('index.php/login');
	   }

    }

	// function getDelivery(){
	// 	$tanggal = $this->input->get('date');
	// 	$data = $this->modelDelivery->getDelivery($tanggal);

    //     echo json_encode($data);
    // }

	public function getDelivery(){
		$date = date('Y-m-d');
		$data = $this->modelDelivery->getDelivery($date);

		$no = 1;
		$delivery = array();

		foreach($data as $r) {

			$delivery_kode = $r->delivery_kode;
			$delivery_email = $r->delivery_email;
			$invoice_no = $r->invoice_no == NULL ? 'Tidak Ada Nomor Invoice' : $r->invoice_no;
			$delivery_tanggal = $r->delivery_tanggal;
			$delivery_customer = $r->delivery_customer;
			$delivery_address = $r->delivery_address;
			$delivery_no = $r->delivery_no;
			$delivery_ket = $r->delivery_ket == NULL ? "Tidak Ada Keterangan" : $r->delivery_ket;
			$total_price = $r->total_price;
			$status = $r->status;
			$delivery_status = $r->delivery_status == NULL ? "Tidak Ada Status Delivery" : $r->delivery_status;
			$delivery_progress = $r->delivery_progress == NULL ? "Tidak Ada Status Delivery Progress" : $r->delivery_progress;

			$action = '
				<a href="delivery/surat?nomor='.$r->delivery_kode.'" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> PRINT</a>
				<a href="delivery/view?nomor='.$r->delivery_kode.'#daftar" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> VIEW</a>
			';
			
			$action .= "
				
				<a href='javascript:;' 
				data-id='$r->id'
				data-delivery_progress='$r->delivery_progress'
				data-toggle='modal' data-target='#edit_delivery'
				class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> EDIT</a>
			";
			
			$delivery[] = array(
				$no++,
				$action,
				$delivery_kode,
				$delivery_status,
				$delivery_progress,
				$delivery_email,
				$invoice_no,
				$delivery_tanggal,
				$delivery_customer,
				$delivery_address,
				$delivery_no,
				$delivery_ket
			);
			
		}
          
		echo json_encode($delivery);
	}

	public function surat(){

		$nomor = $this->input->get('nomor');

		$pdf = new DeliveryPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);

		$pdf->SetTitle('SURAT JALAN');
		$pdf->SetSubject('SURAT JALAN');

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->AddPage();

		$getData = $this->modelDelivery->getData($nomor);

		$html2 = "
			<table>
				<tr>
					<td align=\"right\"><h5>NO : $getData->delivery_kode</h5></td>
				</tr>
				<tr>
					<td align=\"right\"><h5>Tanggal : $getData->delivery_tanggal</h5></td>
				</tr>
			</table>

            <table>	
				<tr>
					<td align=\"left\"><h6>KLIEN</h6></td>
				</tr>
				<tr>
                    <td width=\"25%\"><p style=\"font-size:10px\">Nama</p></td>
					<td width=\"5%\" style=\"font-size:10px\">:</td>
                    <td><p style=\"font-size:10px\">$getData->delivery_customer</p></td>
                </tr>
				<tr>
					<td width=\"25%\"><p style=\"font-size:10px\">Nama Toko</p></td>
					<td width=\"5%\" style=\"font-size:10px\">:</td>
					<td><p style=\"font-size:10px\">$getData->delivery_toko</p></td>
                </tr>
                <tr>
					<td width=\"25%\"><p style=\"font-size:10px\">Alamat</p></td>
					<td width=\"5%\" style=\"font-size:10px\">:</td>
					<td><p style=\"font-size:10px\">$getData->delivery_address</p></td>
                </tr>
				<tr>
					<td width=\"25%\"><p style=\"font-size:10px\">Telepon</p></td>
					<td width=\"5%\" style=\"font-size:10px\">:</td>
					<td><p style=\"font-size:10px\">$getData->delivery_phone</p></td>
                </tr>
			</table>
			<br><br>
			<table>
				<tr>
					<td align=\"left\"><h6>DETAIL</h6></td>
				</tr>
				<tr>
                    <td width=\"25%\"><p style=\"font-size:10px\">Status</p></td>
					<td width=\"5%\" style=\"font-size:10px\">:</td>
                    <td width=\"30%\"><p style=\"font-size:10px\">$getData->delivery_status</p></td>

					<td width=\"12%\"><p style=\"font-size:10px\">No. Polisi</p></td>
					<td width=\"5%\" style=\"font-size:10px\">:</td>
					<td><p style=\"font-size:10px\">$getData->delivery_mobil</p></td>
                </tr>
			</table>
		";

		$html2 .= "
			<p style=\"font-size:10px\">Dikirmkan barang - barang sebagai berikut: </p>
			<table class=\"table\">
				<thead>
					<tr>
						<th width=\"5%\" style=\"font-size:10px\"><b>No</b></th>
						<th width=\"45%\" style=\"font-size:10px\"><b>Nama Barang</b></th>
						<th width=\"10%\" style=\"font-size:10px\"><b>Qty</b></th>
						<th width=\"20%\" style=\"font-size:10px\"><b>Harga</b></th>
						<th width=\"50%\" style=\"font-size:10px\"><b>Total</b></th>
					</tr>
				</thead>
				<tbody>
					
				
		";

		$getDetail = $this->modelDelivery->getDetail($nomor);

		$no = 0;
	
		foreach ($getDetail as $row) {
			$no++;
			
			$harga = $row->price;
			
			if($harga == NULL){
				$harga = "Rp " . number_format(0,2,',','.');
			}else if($harga == ""){
				$harga = "Rp " . number_format(0,2,',','.');
			}else{
				$harga = "Rp " . number_format($row->price,2,',','.');
			}

			$total = $row->total;
			
			if($total == NULL){
				$total = "Rp " . number_format(0,2,',','.');
			}else if($total == ""){
				$total = "Rp " . number_format(0,2,',','.');
			}else{
				$total = "Rp " . number_format($row->total,2,',','.');
			}

			$html2 .= "				
				<tr>
					<th width=\"5%\" style=\"font-size:10px\">$no.</th>
					<th width=\"45%\" style=\"font-size:10px\">$row->sku - $row->product $row->type ($row->size $row->uom)</th>
					<th width=\"10%\" style=\"font-size:10px\">$row->jumlah</th>
					<th width=\"20%\" style=\"font-size:10px\">$harga</th>
					<th width=\"50%\" style=\"font-size:10px\">$total</th>
				</tr>
			";
		}

		$html2 .= "
			</tbody>
		</table>
		";

		$html2 .= "
			<br><br>
			<table>
				<tr>
					<td align=\"left\"><h5>Pengirim</h5></td>
					<td align=\"left\"><h5>Supir</h5></td>
					<td align=\"left\"><h5>Diterima Oleh</h5></td>
				</tr>
			</table>
		";

		$pdf->writeHTML($html2, true, false, false, false, '');

		$namaPDF = 'LAPORANPERWALIAN.pdf';
		$pdf->Output($namaPDF, 'I');
	}

	public function updateDelivery(){

		$this->form_validation->set_rules('id', 'Id', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
			redirect();
		}
		else
		{
			$nomor = $this->input->post('nomor');

			$id = $this->input->post('id');
			$x['jumlah'] = $this->input->post('jumlah');
			$x['price'] = $this->input->post('price') == NULL ? 0 : $this->input->post('price');
			$x['total'] = $x['jumlah'] * $x['price'];

			$this->modelDelivery->updateDelivery($x, $id);

			redirect("delivery/view?nomor=$nomor#daftar");
		}
	}

	public function updateStatusdelivery(){
		$this->form_validation->set_rules('delivery_progress', 'Delivery Progree', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
			redirect("delivery#daftar");
		}
		else
		{
			$id = $this->input->post('id');
			$x['delivery_progress'] = $this->input->post('delivery_progress');
			$x['delivery_progress_up'] = date('Y-m-d H:i:s');

			$this->modelDelivery->updateStatusdelivery($x, $id);

			redirect("delivery#daftar");
		}
	}
}
