<?php

class Delivery_model extends CI_Model{

    public function getConfig(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SSJ')->get()->result();
    }

    public function getConfigProgress(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SSJP')->get()->result();
    }

    public function getDetail($nomor){
        // return $this->db->select('*')->from('delivery_detail')->where('delivery_kode', $nomor)->get()->result();

        return $this->db->select('a.*, b.*')->from('delivery_detail as a')
        ->join('jawara_sku as b', 'a.sku = b.sku')
        ->where('a.delivery_kode', $nomor)->get()->result();
    }

    public function getData($nomor){
        return $this->db->select('*')->from('delivery')->where('delivery_kode', $nomor)->get()->result()[0];
    }

    public function getDelivery($date){
        return $this->db->select('*')->from('delivery')->where('status_reversal IS NULL')->where('delivery_tanggal', $date)->get()->result();
    }

    public function getDeliveryDetail($nomor){
        return $this->db->select('*')->from('delivery_detail')->where('delivery_kode', $nomor)->get()->result();
    }

    public function updateDelivery($data, $id){
        $result = $this->db->where('id', $id)->update('delivery_detail', $data);
        if($result){
            $this->session->set_flashdata('success', 'Data Berhasil diupdate.');
            return TRUE;
        }else{
            $this->session->set_flashdata('success', 'Gagal Update Data.');
            return FALSE;
        }
    }

    public function updateStatusdelivery($data, $id){
        $result = $this->db->where('id', $id)->update('delivery', $data);
        if($result){
            $this->session->set_flashdata('success', 'Data Berhasil diupdate.');
            return TRUE;
        }else{
            $this->session->set_flashdata('success', 'Gagal Update Data.');
            return FALSE;
        }
    }

    // public function getDelivery(){
    //     return $this->db->select('*')->from('delivery')->get()->result();
    // }
}
?>