
	<section class="page-section m-5" id="daftar">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">JApang WArung RAkyat</h4>
				<p class="section-subheading text-muted">Yuk Cetak dan Cek Surat Jalan.</p>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="panel panel-inverse">
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table id="tbl-detail-view" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>ACTION</th>
                                            <th>KODE DELIVERY</th>
                                            <th>SKU</th>
                                            <th>JUMLAH</th>
                                            <th>PRICE</th>
                                            <th>TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                        $no = 0;
                                        foreach($data as $row){
                                            $no++; ?>
                                            <tr>
                                                <td><?= $no ?></td>
                                                <td>
                                                    <a href='javascript:;'
                                                    data-toggle='modal' data-target='#edit-total<?= $row->id ?>'
                                                    class='btn btn-sm btn-warning'><i class='fa fas fa-edit'></i> EDIT</a>
                                                </td>
                                                <td><?= $row->delivery_kode ?></td>
                                                <td><?= $row->sku ?></td>
                                                <td><?= $row->jumlah ?></td>
                                                <td><?= $row->price ?></td>
                                                <td><?= $row->total ?></td>
                                            </tr>

                                            <div class="modal fade" id="edit-total<?= $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog " role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">DEPLOY</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="delivery/updateDelivery" enctype="multipart/form-data" method="POST">
                                                                
                                                                <div class="row">
                                                                    <input class="form-control" type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
                                                                    <input class="form-control" type="hidden" name="nomor" id="nomor" value="<?= $row->delivery_kode ?>"/>
                                                                    
                                                                    <div class="form-group col-xl-4">
                                                                        <label class="col-form-label">Jumlah <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control" name="jumlah" id="jumlah" value="<?= $row->jumlah ?>" autocomplete="off" required>
                                                                    </div>
                                                                    <div class="form-group col-xl-4">
                                                                        <label class="col-form-label">Price <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control" name="price" id="price" value="<?= $row->price ?>" autocomplete="off" readonly>
                                                                    </div>
                                                                    <div class="form-group col-xl-4">
                                                                        <label class="col-form-label">Total <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control" name="total" id="total" value="<?= $row->total ?>" autocomplete="off" readonly>
                                                                    </div>
                                                                </div>
                                                                
                                                                </br>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                                                                    <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end panel-body -->
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-10 -->
            </div>
		</div>
	</section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
         $(document).ready(function() {
            $('#tbl-detail-del').dataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                responsive: true
            });
            
        } );

        $('#jumlah').change(function() {
            var jumlah = $('#jumlah').val();
            var price = $('#price').val();

            jQuery("input#total").val(price*jumlah);

        });
    </script>