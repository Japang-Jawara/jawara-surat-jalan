
	<section class="page-section m-5" id="daftar">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">JApang WArung RAkyat</h4>
				<p class="section-subheading text-muted">Yuk Cetak dan Cek Surat Jalan.</p>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="panel panel-inverse">
                        <div class="panel-body">
                            <div class="table-responsive">

                                <form action="<?= base_url('delivery/detail#daftar') ?>" method="POST">
                                    <div class="row">
                                        <div class="col-xl-2">
                                           <input type="date" class="form-control" name="delivery_tanggall" id="delivery_tanggall" value="<?= $date ?>" required/>
                                        </div>
                                        <div class="col-xl-2">
                                            <button type="submit" class="btn btn-xl btn-primary">Search</button></span>
                                        </div>
                                    </div>
                                </form>
                                
                                <br>
                                <table id="tbl-detail-del" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>ACTION</th>
                                            <th>KODE DELIVERY</th>
                                            <th>DELIVERY STATUS</th>
                                            <th>DELIVERY PROGRESS</th>
                                            <th>EMAIL VERIFIKATOR</th>
                                            <th>INVOICE NO</th>
                                            <th>TANGGAL DELIVERY</th>
                                            <th>NAMA KLIEN</th>
                                            <th>ALAMAT</th>
                                            <th>NOMOR SURAT</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                        $no = 0;
                                        foreach($data as $row){
                                            $no++; ?>
                                            <tr>
                                                <td><?= $no ?></td>
                                                <td>
                                                    <a href="<?= base_url() ?>delivery/surat?nomor=<?= $row->delivery_kode ?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> PRINT</a>
                                                    <a href="<?= base_url() ?>delivery/view?nomor=<?= $row->delivery_kode ?>#daftar" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> VIEW</a>
                                                    <a href="javascript:;" data-id="<?= $row->id ?>" data-delivery_progress="<?= $row->delivery_progress ?>" data-toggle="modal" data-target="#edit_delivery" class="btn btn-sm btn-success"><i class="fa fas fa-edit"></i> EDIT</a>
                                                </td>
                                                <td><?= $row->delivery_kode ?></td>
                                                <td><?= $row->delivery_status == NULL ? "Tidak Ada Status Delivery" : $row->delivery_status ?></td>
                                                <td><?= $row->delivery_progress == NULL ? "Tidak Ada Status Delivery Progress" : $row->delivery_progress ?></td>
                                                <td><?= $row->delivery_email ?></td>
                                                <td><?= $row->invoice_no == NULL ? "Tidak Ada Nomor Invoice" : $row->invoice_no ?></td>
                                                <td><?= $row->delivery_tanggal ?></td>
                                                <td><?= $row->delivery_customer ?></td>
                                                <td><?= $row->delivery_address ?></td>
                                                <td><?= $row->delivery_no ?></td>
                                                <td><?= $row->delivery_ket == NULL ? "Tidak Ada Keterangan" : $row->delivery_ket ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end panel-body -->
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-10 -->
            </div>
		</div>
	</section>

    <div class="modal fade" id="edit_delivery" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Delivery</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('delivery/updateStatusdelivery') ?>" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input class="form-control" type="hidden" name="id" id="id" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Delivery Progress<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="delivery_progressx" id="delivery_progressx" disabled/>
                                <select class="form-control" name="delivery_progress" id="delivery_progress" data-parsley-group="step-1" data-parsley-required="true" required>
                                    <option value="">-- Pilih Status --</option>
                                    <?php foreach($ssjp as $r) { ?>
                                        <option value="<?= $r->name ?>"><?= $r->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                 
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    <script>
         $(document).ready(function() {
            $('#tbl-detail-del').dataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                responsive: true
            });
            
        } );
    </script>