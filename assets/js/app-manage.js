$(document).ready(function() {
    if ($('#tbl-delivery').length !== 0) {

        var url = 'delivery/getDelivery';
        $(document).ready(function() {
            $('#tbl-delivery').dataTable({
                dom: 'Bfrtip',
                // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
                buttons: [
                    'excel'
                ],
                responsive: true,
                "ajax": {
                    "url": url,
                    "dataSrc": ""
                }
            });
            
        } );
    }

    $('#edit_delivery').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        console.log(div.data('delivery_progress'))

        // Isi nilai pada field
        modal.find('#id').attr("value", div.data('id'));
        modal.find('#delivery_progressx').val(div.data('delivery_progress'));
        
    });
});