var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
if (month.length < 2) month = '0' + month;
if (day.length < 2) day = '0' + day;

var tgl = [year, month, day].join('-');
var tanggal = $('#delivery_tanggal').val();

if(tgl == tanggal){

    var url = 'delivery/getDelivery?date='+tanggal;

    $(document).ready(function(){
        tampil_data_surat_jalan();
        
        $('#tbl-delivery').dataTable();

        function tampil_data_surat_jalan(){
            $.ajax({
                type  : 'POST',
                url   : url,
                async : true,
                cache : false,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        var angka = i+1;
                        html += '<tr>'+
                                    '<td>'+angka+'</td>'+
                                    '<td>'+
                                        '<a href="delivery/surat?nomor='+data[i].delivery_kode+'"'+
                                        'target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> PRINT</a>'+
                                    '</td>'+
                                    '<td>'+data[i].delivery_kode+'</td>'+
                                    '<td>'+data[i].delivery_email+'</td>'+
                                    '<td>'+data[i].delivery_tanggal+'</td>'+
                                    '<td>'+data[i].delivery_customer+'</td>'+
                                    '<td>'+data[i].delivery_no+'</td>'+
                                    '<td>'+data[i].status+'</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }

            });
        }

    });
}

$('#delivery_tanggall').change(function() {

    var tanggal = $('#delivery_tanggall').val();
    var url = 'delivery/getDelivery?date='+tanggal;

    $(document).ready(function(){
        tampil_data_surat_jalan();   //pemanggilan fungsi tampil barang.
        
        $('#tbl-delivery').dataTable();
        
        //fungsi tampil barang
        function tampil_data_surat_jalan(){
            $.ajax({
                type  : 'POST',
                url   : url,
                async : true,
                cache : false,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        var angka = i+1;
                        html += '<tr>'+
                                    '<td>'+angka+'</td>'+
                                    '<td>'+
                                        '<a href="delivery/surat?nomor='+data[i].delivery_kode+'"'+
                                        'target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> PRINT</a>'+
                                    '</td>'+
                                    '<td>'+data[i].delivery_kode+'</td>'+
                                    '<td>'+data[i].delivery_email+'</td>'+
                                    '<td>'+data[i].delivery_tanggal+'</td>'+
                                    '<td>'+data[i].delivery_customer+'</td>'+
                                    '<td>'+data[i].delivery_no+'</td>'+
                                    '<td>'+data[i].status+'</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }
            });
        }
    });
});